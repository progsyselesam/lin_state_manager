#ifndef LIN_GENERALTYPES_H
#define LIN_GENERALTYPES_H


/** @file Lin_GeneralTypes.h
 * 
 * @brief Definition of types     
 * 
 * @details This specification specifies functionality, API and configuration \n
 *          of the module LIN transceiver driver. It is responsible to handle \n
 *          the LIN transceiver hardware on an ECU.
 * 
 * @see "Specification of LIN Transceiver Driver" AUTOSAR CP R20-11
 */ 


/** @brief LinTrcv_TrcvModeType    
 * 
 * @details Operating modes of the LIN Transceiver Driver \n
 *          Software Design Specification: [SWS_LinTrcv_00168]
 */ 
typedef enum {
    LINTRCV_TRCV_MODE_NORMAL, 
    LINTRCV_TRCV_MODE_STANDBY,
    LINTRCV_TRCV_MODE_SLEEP
} LinTrcv_TrcvModeType;


#endif /* LIN_GENERALTYPES_H */

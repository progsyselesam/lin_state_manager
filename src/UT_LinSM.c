/** @file UT_LinSM.c
 * 
 * @brief Unit tests for LinSM
 * 
 * @see "Specification of LIN State Manager" AUTOSAR CP R20-11
 */

#include "Std_Types.h"
#include "acutest.h"
#include "fff.h"
#include "LinSM.c"


DEFINE_FFF_GLOBALS;


// LinIf.h
FAKE_VALUE_FUNC(Std_ReturnType, LinIf_GotoSleep, NetworkHandleType);
FAKE_VALUE_FUNC(Std_ReturnType, LinIf_ScheduleRequest, NetworkHandleType, LinIf_SchHandleType);
FAKE_VALUE_FUNC(Std_ReturnType, LinIf_Wakeup, NetworkHandleType);

// ComM_BusSM.h
FAKE_VOID_FUNC(ComM_BusSM_ModeIndication, NetworkHandleType, ComM_ModeType);
FAKE_VOID_FUNC(ComM_BusSM_BusSleepMode, NetworkHandleType);

// BswM_LinSM.h
FAKE_VOID_FUNC(BswM_LinSM_CurrentSchedule, NetworkHandleType, LinIf_SchHandleType);
FAKE_VOID_FUNC(BswM_LinSM_CurrentState, NetworkHandleType, LinSM_ModeType);

// Det.h
FAKE_VALUE_FUNC(Std_ReturnType, Det_ReportError, uint16, uint8, uint8, uint8);
FAKE_VALUE_FUNC(Std_ReturnType, Det_ReportRuntimeError, uint16, uint8, uint8, uint8);

#define FFF_FAKES_LIST(FAKE);	    \
	FAKE(LinIf_GotoSleep)			\
	FAKE(LinIf_ScheduleRequest)		\
	FAKE(LinIf_Wakeup)			    \
	FAKE(ComM_BusSM_ModeIndication)	\
	FAKE(ComM_BusSM_BusSleepMode)	\
	FAKE(BswM_LinSM_CurrentSchedule)\
    FAKE(BswM_LinSM_CurrentState)   \
    FAKE(Det_ReportError)           \
    FAKE(Det_ReportRuntimeError)

/** @brief Test of LinSM_Init function.
 *
 * @details Function test of LinSM initialization. \n
 *          Test of Software Design Specification: [SWS_LinSM_00155]
 */
void Test_Of_LinSM_Init(void)
{
    LinSM_ConfigType * ConfigPtr;
    LinSM_Init(ConfigPtr);

    //Test of Software Design Specification: [SWS_LinSM_00025]
    TEST_CHECK(LinSM_State == LINSM_INIT);

    //Test of Software Design Specification: [SWS_LinSM_00152] [SWS_LinSM_00160]
    TEST_CHECK(LinSM_SubState == LINSM_NO_COM);

    //Test of Software Design Specification: [SWS_LinSM_00216]
    TEST_CHECK(LinSM_ScheduleTable == NULL_SCHEDULE);

    //Test of Software Design Specification: [SWS_LinSM_00043]
    TEST_CHECK(GotoSleepTimer == 0);
    TEST_CHECK(WakeupTimer == 0);
    TEST_CHECK(ScheduleRequestTimer == 0);
}


/** @brief Test of LinSM_ScheduleRequest function.
 *
 * @details Function test of request a schedule table to be changed \n
 *          on one LIN network by upper layer. \n
 *          Test of Software Design Specification: [SWS_LinSM_00113]
 */
void Test_Of_LinSM_ScheduleRequest(void)
{
    Std_ReturnType retv;
    NetworkHandleType network;
    LinIf_SchHandleType schedule;

    //Test of Software Design Specification: [SWS_LinSM_00114]
    FFF_FAKES_LIST(RESET_FAKE);
    network             = 2;
    schedule            = 23;
    LinSM_SubState      = LINSM_NO_COM;
    LinSM_State         = LINSM_INIT;
    LinSM_ScheduleTable = 0;

    retv = LinSM_ScheduleRequest(network, schedule);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_SCHEDULE_REQUEST_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_NONEXISTENT_NETWORK);
    TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
    TEST_CHECK(retv == E_NOT_OK);

    //Test of Software Design Specification: [SWS_LinSM_00115]
    FFF_FAKES_LIST(RESET_FAKE);
    network         = 1;
    schedule        = NULL_SCHEDULE;
    LinSM_SubState  = LINSM_NO_COM;
    LinSM_State     = LINSM_INIT;
    LinSM_ScheduleTable = 0;

    retv = LinSM_ScheduleRequest(network, schedule);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_SCHEDULE_REQUEST_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_PARAMETER);
    TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
    TEST_CHECK(retv == E_NOT_OK);

    //Test of Software Design Specification: [SWS_LinSM_00116]
    FFF_FAKES_LIST(RESET_FAKE);
    network             = 1;
    schedule            = 23;
    LinSM_SubState      = LINSM_NO_COM;
    LinSM_State         = LINSM_UNINIT;
    LinSM_ScheduleTable = 0;

    retv = LinSM_ScheduleRequest(network, schedule);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_SCHEDULE_REQUEST_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_UNINIT);
    TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
    TEST_CHECK(retv == E_NOT_OK);

    //Test of Software Design Specification: [SWS_LinSM_10211]
    FFF_FAKES_LIST(RESET_FAKE);
    network             = 1;
    schedule            = 23;
    LinSM_SubState      = LINSM_NO_COM;
    LinSM_State         = LINSM_INIT;
    LinSM_ScheduleTable = 0;

    retv = LinSM_ScheduleRequest(network, schedule);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
    TEST_CHECK(retv == E_NOT_OK);

    //Test of Software Design Specification: [SWS_LinSM_00100]
    FFF_FAKES_LIST(RESET_FAKE);
    network             = 1;
    schedule            = 23;
    LinSM_SubState      = LINSM_FULL_COM;
    LinSM_State         = LINSM_INIT;
    LinSM_ScheduleTable = 0;
    LinIf_ScheduleRequest_fake.return_val = E_OK;

    retv = LinSM_ScheduleRequest(network, schedule);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 1);
    TEST_CHECK(LinIf_ScheduleRequest_fake.arg0_val == network);
    TEST_CHECK(LinIf_ScheduleRequest_fake.arg1_val == schedule);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
    TEST_CHECK(retv == E_OK);

    //Test of Software Design Specification: [SWS_LinSM_00213]
    FFF_FAKES_LIST(RESET_FAKE);
    network             = 1;
    schedule            = 23;
    LinSM_SubState      = LINSM_FULL_COM;
    LinSM_State         = LINSM_INIT;
    LinSM_ScheduleTable = 0;
    LinIf_ScheduleRequest_fake.return_val = E_NOT_OK;

    retv = LinSM_ScheduleRequest(network, schedule);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 1);
    TEST_CHECK(LinIf_ScheduleRequest_fake.arg0_val == network);
    TEST_CHECK(LinIf_ScheduleRequest_fake.arg1_val == schedule);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 1);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg0_val == network);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg1_val == LinSM_ScheduleTable);
    TEST_CHECK(retv == E_NOT_OK);

}


/** @brief Test of LinSM_GetVersionInfo function.
 *
 * @details Function test of get version info. \n
 *          Test of Software Design Specification: [SWS_LinSM_00117]
 */
void Test_Of_LinSM_GetVersionInfo(void)
{
    //Test of Software Design Specification: [SWS_LinSM_00119]
    FFF_FAKES_LIST(RESET_FAKE);

    LinSM_GetVersionInfo(NULL);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_GET_VERSION_INFO_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_PARAM_POINTER);

}


/** @brief Test of LinSM_GetCurrentComMode function.
 *
 * @details Function test of query the current communication mode. \n
 *          Test of Software Design Specification: [SWS_LinSM_00122]
 */
void Test_Of_LinSM_GetCurrentComMode(void)
{
    Std_ReturnType retv;
    NetworkHandleType network;
    ComM_ModeType mode;


    // Test of Software Design Specification: [SWS_LinSM_00123]
    FFF_FAKES_LIST(RESET_FAKE);
    network         = 12;
    LinSM_State     = LINSM_UNINIT;
    LinSM_SubState  = LINSM_NO_COM;

    retv = LinSM_GetCurrentComMode(network, &mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_GET_CURR_COMM_MOD_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_NONEXISTENT_NETWORK);
    TEST_CHECK(retv == E_NOT_OK);


    // Test of Software Design Specification: [SWS_LinSM_00124]
    FFF_FAKES_LIST(RESET_FAKE);
    network         = 1;
    LinSM_State     = LINSM_UNINIT;
    LinSM_SubState  = LINSM_NO_COM;

    retv = LinSM_GetCurrentComMode(network, NULL);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_GET_CURR_COMM_MOD_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_PARAM_POINTER);
    TEST_CHECK(retv == E_NOT_OK);


    //Test of Software Design Specification: [SWS_LinSM_00125][SWS_LinSM_00182]
    FFF_FAKES_LIST(RESET_FAKE);
    network         = 1;
    LinSM_State     = LINSM_UNINIT;
    LinSM_SubState  = LINSM_NO_COM;

    retv = LinSM_GetCurrentComMode(network, &mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_GET_CURR_COMM_MOD_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_UNINIT);
    TEST_CHECK(mode == COMM_NO_COMMUNICATION);
    TEST_CHECK(retv == E_NOT_OK);


    //Test of Software Design Specification: [SWS_LinSM_00180]
    FFF_FAKES_LIST(RESET_FAKE);
    network         = 1;
    LinSM_State     = LINSM_INIT;
    LinSM_SubState  = LINSM_NO_COM;

    retv = LinSM_GetCurrentComMode(network, &mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(mode == COMM_NO_COMMUNICATION);
    TEST_CHECK(retv == E_OK);


    //Test of Software Design Specification: [SWS_LinSM_00181]
    FFF_FAKES_LIST(RESET_FAKE);
    network         = 1;
    LinSM_State     = LINSM_INIT;
    LinSM_SubState  = LINSM_FULL_COM;

    retv = LinSM_GetCurrentComMode(network, &mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(mode == COMM_FULL_COMMUNICATION);
    TEST_CHECK(retv == E_OK);
}


/** @brief Test of LinSM_RequestComMode function.
 *
 * @details Function test of requesting of a communication mode. \n
 *          Test of Software Design Specification: [SWS_LinSM_00126]
 */
void Test_Of_LinSM_RequestComMode(void)
{
    Std_ReturnType retv;
    NetworkHandleType network;
    ComM_ModeType mode;


    // Test of Software Design Specification: [SWS_LinSM_00127]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 46;
    mode                    = COMM_NO_COMMUNICATION;
    LinSM_State             = LINSM_UNINIT;
    LinSM_SubState          = LINSM_NO_COM;
    LinSM_Full_Com_SubState = LINSM_RUN_COMMUNICATION;

    retv = LinSM_RequestComMode(network, mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_REQ_COMM_MODE_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_NONEXISTENT_NETWORK);
    TEST_CHECK(LinIf_GotoSleep_fake.call_count == 0);
    TEST_CHECK(LinIf_Wakeup_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(retv == E_NOT_OK);


    // Test of Software Design Specification: [SWS_LinSM_00191]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    mode                    = 4;
    LinSM_State             = LINSM_UNINIT;
    LinSM_SubState          = LINSM_NO_COM;
    LinSM_Full_Com_SubState = LINSM_RUN_COMMUNICATION;

    retv = LinSM_RequestComMode(network, mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_REQ_COMM_MODE_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_PARAMETER);
    TEST_CHECK(LinIf_GotoSleep_fake.call_count == 0);
    TEST_CHECK(LinIf_Wakeup_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(retv == E_NOT_OK);


    // Test of Software Design Specification: [SWS_LinSM_00128]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    mode                    = COMM_NO_COMMUNICATION;
    LinSM_State             = LINSM_UNINIT;
    LinSM_SubState          = LINSM_NO_COM;
    LinSM_Full_Com_SubState = LINSM_RUN_COMMUNICATION;

    retv = LinSM_RequestComMode(network, mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_REQ_COMM_MODE_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_UNINIT);
    TEST_CHECK(LinIf_GotoSleep_fake.call_count == 0);
    TEST_CHECK(LinIf_Wakeup_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(retv == E_NOT_OK);


    // Test of Software Design Specification: [SWS_LinSM_00183]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    mode                    = COMM_SILENT_COMMUNICATION;
    LinSM_State             = LINSM_INIT;
    LinSM_SubState          = LINSM_NO_COM;
    LinSM_Full_Com_SubState = LINSM_RUN_COMMUNICATION;

    retv = LinSM_RequestComMode(network, mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(LinIf_GotoSleep_fake.call_count == 0);
    TEST_CHECK(LinIf_Wakeup_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(retv == E_NOT_OK);


    // Test of Software Design Specification: [SWS_LinSM_00035] [SWS_LinSM_00036] [SWS_LinSM_10208][SWS_LinSM_00100][SWS_LinSM_00302]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    mode                    = COMM_NO_COMMUNICATION;
    LinSM_State             = LINSM_INIT;
    LinSM_SubState          = LINSM_FULL_COM;
    LinSM_Full_Com_SubState = LINSM_RUN_COMMUNICATION;
    LinIf_GotoSleep_fake.return_val = E_OK;

    retv = LinSM_RequestComMode(network, mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(LinIf_GotoSleep_fake.call_count == 1);
    TEST_CHECK(LinIf_GotoSleep_fake.arg0_val == network);
    TEST_CHECK(LinIf_Wakeup_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(GotoSleepTimer == LINSM_CONFIRMATION_TIMEOUT);
    TEST_CHECK(LinSM_Full_Com_SubState == LINSM_GOTO_SLEEP);
    TEST_CHECK(retv == E_OK);


    // Test of Software Design Specification: [SWS_LinSM_00177] [SWS_LinSM_00100]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    mode                    = COMM_NO_COMMUNICATION;
    LinSM_State             = LINSM_INIT;
    LinSM_SubState          = LINSM_FULL_COM;
    LinSM_Full_Com_SubState = LINSM_RUN_COMMUNICATION;
    LinIf_GotoSleep_fake.return_val = E_NOT_OK;

    retv = LinSM_RequestComMode(network, mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(LinIf_GotoSleep_fake.call_count == 1);
    TEST_CHECK(LinIf_GotoSleep_fake.arg0_val == network);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg0_val == network);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg1_val == COMM_FULL_COMMUNICATION);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 1);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.arg0_val == network);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.arg1_val == LINSM_FULL_COM);
    TEST_CHECK(LinIf_Wakeup_fake.call_count == 0);
    TEST_CHECK(GotoSleepTimer == 0);
    TEST_CHECK(retv == E_NOT_OK);


    // Test of Software Design Specification: [SWS_LinSM_00047][SWS_LinSM_00100]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    mode                    = COMM_FULL_COMMUNICATION;
    LinSM_State             = LINSM_INIT;
    LinSM_SubState          = LINSM_FULL_COM;
    LinSM_Full_Com_SubState = LINSM_RUN_COMMUNICATION;
    LinIf_Wakeup_fake.return_val = E_OK;

    retv = LinSM_RequestComMode(network, mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(LinIf_GotoSleep_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(LinIf_Wakeup_fake.call_count == 1);
    TEST_CHECK(LinIf_Wakeup_fake.arg0_val == network);
    TEST_CHECK(WakeupTimer == LINSM_CONFIRMATION_TIMEOUT);
    TEST_CHECK(retv == E_OK);


    // // Test of Software Design Specification: [SWS_LinSM_00176][SWS_LinSM_00100]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    mode                    = COMM_FULL_COMMUNICATION;
    LinSM_State             = LINSM_INIT;
    LinSM_SubState          = LINSM_FULL_COM;
    LinSM_Full_Com_SubState = LINSM_RUN_COMMUNICATION;
    LinIf_Wakeup_fake.return_val = E_NOT_OK;

    retv = LinSM_RequestComMode(network, mode);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(LinIf_GotoSleep_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(LinIf_Wakeup_fake.call_count == 1);
    TEST_CHECK(LinIf_Wakeup_fake.arg0_val == network);
    TEST_CHECK(WakeupTimer == 0);
    TEST_CHECK(retv == E_NOT_OK);
}


/** @brief Test of LinSM_ScheduleRequestConfirmation function.
 *
 * @details Function test of callback by LinIf module \n
 *          when the new requested schedule table is active. \n
 *          Test of Software Design Specification: [SWS_LinSM_00129]
 */
void Test_Of_LinSM_ScheduleRequestConfirmation(void)
{
    NetworkHandleType network;
    LinIf_SchHandleType schedule;


    // Test of Software Design Specification: [SWS_LinSM_00130][SWS_LinSM_00207]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 2;
    schedule                = 34;
    LinSM_State             = LINSM_UNINIT;

    LinSM_ScheduleRequestConfirmation(network, schedule);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_SCHEDULE_REQ_CONF_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_NONEXISTENT_NETWORK);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);


    // Test of Software Design Specification: [SWS_LinSM_00131]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    schedule                = 34;
    LinSM_State             = LINSM_UNINIT;

    LinSM_ScheduleRequestConfirmation(network, schedule);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_SCHEDULE_REQ_CONF_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_UNINIT);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);


    // Test of Software Design Specification: [SWS_LinSM_00206][SWS_LinSM_00207][SWS_LinSM_00154]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    schedule                = 34;
    LinSM_State             = LINSM_INIT;
    ScheduleRequestTimer    = 12;

    LinSM_ScheduleRequestConfirmation(network, schedule);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 1);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg0_val == network);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg1_val == schedule);
    TEST_CHECK(LinSM_ScheduleTable == schedule);
    TEST_CHECK(ScheduleRequestTimer == 0);
}


/** @brief Test of LinSM_GotoSleepIndication function.
 *
 * @details Function test of callback by LinIf module \n
 *          when the go to sleep command is received \n
 *          on the network or a bus idle timeout occurs. \n
 *          Test of Software Design Specification: [SWS_LinSM_91000]
 */
void Test_Of_LinSM_GotoSleepIndication(void)
{
    NetworkHandleType Channel;

    // Test of Software Design Specification: [SWS_LinSM_00130][SWS_LinSM_00207]
    FFF_FAKES_LIST(RESET_FAKE);
    Channel                 = 2;
    LinSM_State             = LINSM_UNINIT;

    LinSM_GotoSleepIndication(Channel);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_GOTO_SLEEP_IND_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_NONEXISTENT_NETWORK);;


    // Test of Software Design Specification: [SWS_LinSM_00131]
    FFF_FAKES_LIST(RESET_FAKE);
    Channel                 = 1;
    LinSM_State             = LINSM_UNINIT;

    LinSM_GotoSleepIndication(Channel);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_GOTO_SLEEP_IND_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_UNINIT);
}


/** @brief Test of LinSM_GotoSleepConfirmation function.
 *
 * @details Function test of callback by LinIf module \n
 *          when the go to sleep command is sent successfully \n
 *          or not sent successfully on the network. \n
 *          Test of Software Design Specification: [SWS_LinSM_00135]
 */
void Test_Of_LinSM_GotoSleepConfirmation(void)
{
    NetworkHandleType network;

    //Test of Software Design Specification: [SWS_LinSM_00136]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 2;
    LinSM_SubState          = LINSM_FULL_COM;
    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP;
    LinSM_State             = LINSM_INIT;
    GotoSleepTimer          = 10;

    LinSM_GotoSleepConfirmation(network,TRUE);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_GOTO_SLEEP_CONF_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_NONEXISTENT_NETWORK);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(GotoSleepTimer == 10);
    
    //Test of Software Design Specification: [SWS_LinSM_00137]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    LinSM_SubState          = LINSM_FULL_COM;
    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP;
    LinSM_State             = LINSM_UNINIT;
    GotoSleepTimer          = 10;

    LinSM_GotoSleepConfirmation(network,TRUE);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_GOTO_SLEEP_CONF_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_UNINIT);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(GotoSleepTimer == 10);
    
    //Test of Software Design Specification: [SWS_LinSM_00046]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    LinSM_SubState          = LINSM_FULL_COM;
    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP;
    LinSM_State             = LINSM_INIT;
    GotoSleepTimer          = 0;

    LinSM_GotoSleepConfirmation(network,TRUE);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(GotoSleepTimer == 0);
    
    //Test of Software Design Specification: [SWS_LinSM_00046]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    LinSM_SubState          = LINSM_NO_COM;
    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP;
    LinSM_State             = LINSM_INIT;
    GotoSleepTimer          = 10;

    LinSM_GotoSleepConfirmation(network,TRUE);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(GotoSleepTimer == 0);
    
    //Test of Software Design Specification: [SWS_LinSM_00046]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    LinSM_SubState          = LINSM_FULL_COM;
    LinSM_Full_Com_SubState = LINSM_RUN_COMMUNICATION;
    LinSM_State             = LINSM_INIT;
    GotoSleepTimer          = 10;

    LinSM_GotoSleepConfirmation(network,TRUE);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(GotoSleepTimer == 0);
    
    //Test of Software Design Specification: [SWS_LinSM_00027], [SWS_LinSM_00193]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    LinSM_SubState          = LINSM_FULL_COM;
    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP;
    LinSM_State             = LINSM_INIT;
    GotoSleepTimer          = 10;

    LinSM_GotoSleepConfirmation(network,TRUE);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg0_val == network);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg1_val == COMM_NO_COMMUNICATION);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 1);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.arg0_val == network);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.arg1_val == LINSM_NO_COM);
    TEST_CHECK(LinSM_SubState == LINSM_NO_COM);
    TEST_CHECK(GotoSleepTimer == 0);

}


/** @brief Test of LinSM_WakeupConfirmation function.
 *
 * @details Function test of callback by LinIf module \n
 *          when the wake up signal command is sent \n
 *          not successfully/successfully on the network. \n
 *          Test of Software Design Specification: [SWS_LinSM_00132]
 */
void Test_Of_LinSM_WakeupConfirmation(void)
{
    NetworkHandleType network;

    //Test of Software Design Specification: [SWS_LinSM_00133]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 2;
    LinSM_SubState          = LINSM_NO_COM;
    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP;
    LinSM_State             = LINSM_INIT;
    WakeupTimer             = 10;

    LinSM_WakeupConfirmation(network,TRUE);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_WAKEUP_CONF_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_NONEXISTENT_NETWORK);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(WakeupTimer == 10);
    
    //Test of Software Design Specification: [SWS_LinSM_00134]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    LinSM_SubState          = LINSM_NO_COM;
    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP;
    LinSM_State             = LINSM_UNINIT;
    WakeupTimer             = 10;

    LinSM_WakeupConfirmation(network,TRUE);
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_WAKEUP_CONF_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_UNINIT);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(WakeupTimer == 10);
    
    //Test of Software Design Specification: [SWS_LinSM_00049] [SWS_LinSM_00301] [SWS_LinSM_00033] [SWS_LinSM_00192]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    LinSM_SubState          = LINSM_NO_COM;
    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP;
    LinSM_State             = LINSM_INIT;
    WakeupTimer             = 10;

    LinSM_WakeupConfirmation(network,TRUE);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg0_val == network);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg1_val == COMM_FULL_COMMUNICATION);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 1);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.arg0_val == network);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.arg1_val == LINSM_FULL_COM);
    TEST_CHECK(LinSM_SubState == LINSM_FULL_COM);
    TEST_CHECK(LinSM_Full_Com_SubState == LINSM_RUN_COMMUNICATION);
    TEST_CHECK(WakeupTimer == 0);
    
    //Test of Software Design Specification: [SWS_LinSM_00202]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    LinSM_SubState          = LINSM_NO_COM;
    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP;
    LinSM_State             = LINSM_INIT;
    WakeupTimer             = 10;

    LinSM_WakeupConfirmation(network,FALSE);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg0_val == network);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg1_val == COMM_NO_COMMUNICATION);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 1);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.arg0_val == network);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.arg1_val == LINSM_NO_COM);
    TEST_CHECK(LinSM_SubState == LINSM_NO_COM);
    TEST_CHECK(LinSM_Full_Com_SubState == LINSM_GOTO_SLEEP);
    TEST_CHECK(WakeupTimer == 0);
    
    //Test of Software Design Specification: [SWS_LinSM_00154]
    FFF_FAKES_LIST(RESET_FAKE);
    network                 = 1;
    LinSM_SubState          = LINSM_NO_COM;
    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP;
    LinSM_State             = LINSM_INIT;
    WakeupTimer             = 0;

    LinSM_WakeupConfirmation(network,TRUE);
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(LinSM_SubState == LINSM_NO_COM);
    TEST_CHECK(LinSM_Full_Com_SubState == LINSM_GOTO_SLEEP);
    TEST_CHECK(WakeupTimer == 0);
}

/** @brief Test of LinSM_MainFunction function. 
 *
 * @details Function tesf of Periodic function that runs \n
 *          the timers of different request timeouts. \n
 *          Test of Software Design Specification: [SWS_LinSM_00156]
 */
void Test_Of_LinSM_MainFunction(void)
{

    //Test of Software Design Specification: [SWS_LinSM_00101]
    FFF_FAKES_LIST(RESET_FAKE);
    LinSM_ScheduleTable     = 2;
    ScheduleRequestTimer    = 10;
    GotoSleepTimer          = 20;
    WakeupTimer             = 30;

    LinSM_MainFunction();
    TEST_CHECK(Det_ReportError_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(ScheduleRequestTimer == 9);
    TEST_CHECK(GotoSleepTimer == 19);
    TEST_CHECK(WakeupTimer == 29);

    //Test of Software Design Specification: [SWS_LinSM_00102] [SWS_LinSM_00214]
    FFF_FAKES_LIST(RESET_FAKE);
    LinSM_ScheduleTable     = 2;
    ScheduleRequestTimer    = 0;
    GotoSleepTimer          = 20;
    WakeupTimer             = 30;

    LinSM_MainFunction();
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_MAIN_FUNCTION_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_CONFIRMATION_TIMEOUT);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 1);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg0_val == 1);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg1_val == LinSM_ScheduleTable);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(ScheduleRequestTimer == 0);
    TEST_CHECK(GotoSleepTimer == 19);
    TEST_CHECK(WakeupTimer == 29);

    //Test of Software Design Specification: [SWS_LinSM_00102]
    FFF_FAKES_LIST(RESET_FAKE);
    LinSM_ScheduleTable     = 2;
    ScheduleRequestTimer    = 10;
    GotoSleepTimer          = 0;
    WakeupTimer             = 30;

    LinSM_MainFunction();
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_MAIN_FUNCTION_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_CONFIRMATION_TIMEOUT);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
    TEST_CHECK(ScheduleRequestTimer == 9);
    TEST_CHECK(GotoSleepTimer == 0);
    TEST_CHECK(WakeupTimer == 29);

    //Test of Software Design Specification: [SWS_LinSM_00101] [SWS_LinSM_00102] [SWS_LinSM_00215] [SWS_LinSM_00170]
    FFF_FAKES_LIST(RESET_FAKE);
    LinSM_ScheduleTable     = 2;
    ScheduleRequestTimer    = 10;
    GotoSleepTimer          = 20;
    WakeupTimer             = 0;

    LinSM_MainFunction();
    TEST_CHECK(Det_ReportError_fake.call_count == 1);
    TEST_CHECK(Det_ReportError_fake.arg2_val == LINSM_MAIN_FUNCTION_SERVICE_ID);
    TEST_CHECK(Det_ReportError_fake.arg3_val == LINSM_E_CONFIRMATION_TIMEOUT);
    TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg0_val == 1);
    TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg1_val == COMM_NO_COMMUNICATION);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 1);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.arg0_val == 1);
    TEST_CHECK(BswM_LinSM_CurrentState_fake.arg1_val == LINSM_NO_COM);
    TEST_CHECK(ScheduleRequestTimer == 9);
    TEST_CHECK(GotoSleepTimer == 19);
    TEST_CHECK(WakeupTimer == 0);

}

/*
  List of tests.
*/
TEST_LIST = {
    { "Test of LinSM_Init",                         Test_Of_LinSM_Init                          },
    { "Test of LinSM_ScheduleRequest",              Test_Of_LinSM_ScheduleRequest               },
    { "Test of LinSM_GetVersionInfo",               Test_Of_LinSM_GetVersionInfo                },
    { "Test of LinSM_GetCurrentComMode",            Test_Of_LinSM_GetCurrentComMode             },
    { "Test of LinSM_RequestComMode",               Test_Of_LinSM_RequestComMode                },
    { "Test of LinSM_ScheduleRequestConfirmation",  Test_Of_LinSM_ScheduleRequestConfirmation   },
    { "Test of LinSM_GotoSleepIndication",          Test_Of_LinSM_GotoSleepIndication           },
    { "Test of LinSM_GotoSleepConfirmation",        Test_Of_LinSM_GotoSleepConfirmation         },
    { "Test of LinSM_WakeupConfirmation",           Test_Of_LinSM_WakeupConfirmation            },
    { "Test of LinSM_MainFunction",                 Test_Of_LinSM_MainFunction                  },
    { NULL, NULL }
};

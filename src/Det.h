#ifndef DET_H
#define DET_H


/** @file Det.h
 *
 * @brief Function declaration
 *
 * @details This specification describes the API of the Default Error Tracer. \n
 *          All detected development and runtime errors in the Basic Software \n
 *          are reported to this module.
 *
 * @see "Specification of Default Error Tracer" AUTOSAR CP R20-11
 */


#include "Std_Types.h"


/** @brief Det_ReportError
 *
 * @details Service to report development errors. \n
 *          Software Design Specification: [SWS_Det_00009]
 *
 * @param [in]  ModuleId    Module ID of calling module
 * @param [in]  InstanceId  The identifier of the index based instance of a module, \n
 *                          starting from 0, If the module is a single instance module \n
 *                          it shall pass 0 as the InstanceId.
 * @param [in]  ApiId       ID of API service in which error is detected (defined in SWS of calling module)
 * @param [in]  ErrorId     ID of detected development error (defined in SWS of calling module).
 * 
 * @return never returns a value, but has a return type for compatibility with services and hooks
 */
Std_ReturnType Det_ReportError (
    uint16 ModuleId,
    uint8 InstanceId,
    uint8 ApiId,
    uint8 ErrorId
);


/** @brief Det_ReportRuntimeError
 *
 * @details Service to report runtime errors. If a callout has been configured \n
 *          then this callout shall be called. \n
 *          Software Design Specification: [SWS_Det_01001]
 *
 * @param [in]  ModuleId    Module ID of calling module
 * @param [in]  InstanceId  The identifier of the index based instance of a module, \n
 *                          starting from 0, If the module is a single instance module \n
 *                          it shall pass 0 as the InstanceId.
 * @param [in]  ApiId       ID of API service in which error is detected (defined in SWS of calling module)
 * @param [in]  ErrorId     ID of detected runtime error (defined in SWS of calling module).
 * 
 * @return  E_OK always (is required for services)
 */
Std_ReturnType Det_ReportRuntimeError (
    uint16 ModuleId,
    uint8 InstanceId,
    uint8 ApiId,
    uint8 ErrorId
);




#endif /* DET_H */

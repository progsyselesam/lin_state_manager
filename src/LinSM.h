#ifndef LINSM_H
#define LINSM_H


/** @file LinSM.h
 * 
 * @brief LIN State Manager
 * 
 * @details This specification specifies the functionality, API and the configuration \n
 *          of the AUTOSAR Basic Software module LIN State Manager (LinSM). \n
 *          The LinSM together with the LIN Interface, LIN driver, \n
 *          LIN Transceiver driver forms the complete LIN protocol.
 *          Software Design Specification: [SWS_LinSM_00005]
 * 
 * @see "Specification of LIN State Manager" AUTOSAR CP R20-11
 */ 


/* [SWS_LinSM_00219] */
#include "Rte_ComM_Type.h"
#include "ComStack_Types.h"
#include "LinIf.h"
#include "Lin_GeneralTypes.h"
#include "Std_Types.h"
#include "Det.h"


//SERVICE ID definitions
#define LINSM_SCHEDULE_REQUEST_SERVICE_ID   0x10
#define LINSM_GET_VERSION_INFO_SERVICE_ID   0x02
#define LINSM_GET_CURR_COMM_MOD_SERVICE_ID  0x11
#define LINSM_REQ_COMM_MODE_SERVICE_ID      0x12
#define LINSM_MAIN_FUNCTION_SERVICE_ID      0x30
#define LINSM_SCHEDULE_REQ_CONF_SERVICE_ID  0x20
#define LINSM_GOTO_SLEEP_IND_SERVICE_ID     0x03
#define LINSM_GOTO_SLEEP_CONF_SERVICE_ID    0x22
#define LINSM_WAKEUP_CONF_SERVICE_ID        0x21


/** @brief LinSM_StateType
 * 
 * @details Type used to store current state of the state machine. \n
 *          Software Design Specification: [SWS_LinSM_00020]
 */ 
typedef enum 
{
    LINSM_UNINIT,
    LINSM_INIT
} LinSM_StateType;


// /** @brief LinSM_SubStateType
//  * 
//  * @details Type used to store current state of the sub state machine. \n
//  *          Software Design Specification: [SWS_LinSM_00173]
//  */ 
// typedef enum {
//     LINSM_FULL_COM,
//     LINSM_NO_COM
// } LinSM_SubStateType;


/** @brief LinSM_ModeType
 * 
 * @details Type used to report the current mode to the BswM. \n
 *          Software Design Specification: [SWS_LinSM_00220]
 */ 
typedef enum 
{
    LINSM_FULL_COM  = 0x01,
    LINSM_NO_COM    = 0x02
} LinSM_ModeType;


/** @brief LinSM_Full_Com_SubStateType
 * 
 * @details Type used to store current sub state in the LINSM_FULLCOM state. \n
 *          Software Design Specification: []
 */ 
typedef enum 
{
    LINSM_RUN_COMMUNICATION,
    LINSM_GOTO_SLEEP
} LinSM_Full_Com_SubStateType;


/** @brief LinSMNodeType
 * 
 * @details Type used to store configuration parameters, node type - master or slave. \n
 *          Software Design Specification: [SWS_LinSM_00241]
 */ 
typedef enum 
{
    NODE_MASTER  = 0x01,
    NODE_SLAVE   = 0x02
} LinSMNodeType;


/** @brief LinSM_ConfigType
 * 
 * @details Data structure type for the post-build configuration parameters. \n
 *          Software Design Specification: [SWS_LinSM_00221]
 */ 
typedef struct
{
    LinSM_ModeType LinSM_Mode;
    LinSMNodeType LinSM_Node;
} LinSM_ConfigType;


/** @brief LinSM_Development_ErrorType
 * 
 * @details Type used to development error handling. \n
 *          Software Design Specification: [SWS_LinSM_00053]
 * 
 * @param LINSM_E_UNINIT                - API called without initialization of LinSM
 * @param LINSM_E_NONEXISTENT_NETWORK   - Referenced network does not exist (identification is out of range)
 * @param LINSM_E_PARAMETER             - API service called with wrong parameter
 * @param LINSM_E_PARAM_POINTER         - API service called with invalid pointer
 * @param LINSM_E_INIT_FAILED           - Init function failed
 */ 
typedef enum 
{
    LINSM_E_UNINIT = 0x00,
    LINSM_E_NONEXISTENT_NETWORK = 0x20, 
    LINSM_E_PARAMETER = 0x30, 
    LINSM_E_PARAM_POINTER = 0x40, 
    LINSM_E_INIT_FAILED = 0x50
} LinSM_Development_ErrorType;


/** @brief LinSM_Runtime_ErrorType
 * 
 * @details Type used to runtime error handling. \n
 *          Software Design Specification: [SWS_LinSM_00224]
 * 
 * @param LINSM_E_CONFIRMATION_TIMEOUT  - Timeout of the callbacks from LinIf
 */ 
typedef enum 
{
    LINSM_E_CONFIRMATION_TIMEOUT = 0x00
} LinSM_Runtime_ErrorType;

#include "ComM.h"       /* [SWS_LinSM_00013] */
#include "BswM_LinSM.h" /* [SWS_LinSM_00201] */
#include "ComM_BusSM.h" /* [SWS_LinSM_00305] */

/** @brief LinSM_Init
 * 
 * @details This function initializes the LinSM. \n
 *          Software Design Specification: [SWS_LinSM_00155]
 * 
 * @param [in]  ConfigPtr pointer to the LinSM post-build configuration data.
 */ 
void LinSM_Init (
	const LinSM_ConfigType* ConfigPtr
);

/** @brief LinSM_ScheduleRequest
 * 
 * @details The upper layer requests a schedule table to be changed on one LIN network. \n
 *          Software Design Specification: [SWS_LinSM_00113]
 * 
 * @param [in]  network     identification of the LIN channel
 * @param [in]  schedule    pointer to the new Schedule table
 * 
 * @return  E_OK        Schedule table request has been accepted. \n
 *          E_NOT_OK    Not possible to perform the request, e.g. not initialized.
 */ 
Std_ReturnType LinSM_ScheduleRequest (
	NetworkHandleType network,
	LinIf_SchHandleType schedule
);

/** @brief LinSM_GetVersionInfo
 * 
 * @details Software Design Specification: [SWS_LinSM_00117]
 * 
 * @param [out]  versioninfo    pointer to where to store the version information of this module.
 */ 
void LinSM_GetVersionInfo (
	Std_VersionInfoType* versioninfo
);

/** @brief LinSM_GetCurrentComMode
 * 
 * @details Function to query the current communication mode. \n
 *          Software Design Specification: [SWS_LinSM_00122]
 * 
 * @param [in]  network identification of the LIN channel
 * @param [out] mode    returns the active mode, see ComM_ModeType for descriptions of the modes
 * 
 * @return      E_OK        Ok \n
 *              E_NOT_OK    Not possible to perform the request, e.g. not initialized.
 */
Std_ReturnType LinSM_GetCurrentComMode (
	NetworkHandleType network,
	ComM_ModeType* mode
);

/** @brief LinSM_RequestComMode
 * 
 * @details Requesting of a communication mode. \n
 *          The mode switch will not be made instant. \n 
 *          The LinSM will notify the caller when mode transition is made. \n
 *          Software Design Specification: [SWS_LinSM_00126]
 * 
 * @param [in]  network identification of the LIN channel
 * @param [in]  mode    request mode
 * 
 * @return      E_OK        Request accepted \n
 *              E_NOT_OK    Not possible to perform the request, e.g. not initialized.
 */
Std_ReturnType LinSM_RequestComMode (
	NetworkHandleType network,
	ComM_ModeType mode
);

/** @brief LinSM_ScheduleRequestConfirmation
 * 
 * @details The LinIf module will call this callback \n
 *          when the new requested schedule table is active. \n
 *          Software Design Specification: [SWS_LinSM_00129]
 * 
 * @param [in]  network     identification of the LIN channel
 * @param [in]  schedule    pointer to the new active Schedule table
 */
void LinSM_ScheduleRequestConfirmation (
	NetworkHandleType network,
	LinIf_SchHandleType schedule
);

/** @brief LinSM_GotoSleepIndication
 * 
 * @details The LinIf will call this callback when the go to sleep command \n
 *          is received on the network or a bus idle timeout occurs. \n
 *          Only applicable for LIN slave nodes. \n
 *          Software Design Specification: [SWS_LinSM_91000]
 * 
 * @param [in]  Channel     identification of the LIN channel
 */
void LinSM_GotoSleepIndication (
	NetworkHandleType Channel
);

/** @brief LinSM_GotoSleepConfirmation
 * 
 * @details The LinIf will call this callback when the go to sleep command \n
 *          is sent successfully or not sent successfully on the network. \n
 *          Software Design Specification: [SWS_LinSM_00135]
 * 
 * @param [in]  network identification of the LIN channel
 * @param [in]  success true if goto sleep was successfully sent, false otherwise
 */
void LinSM_GotoSleepConfirmation (
	NetworkHandleType network,
	boolean success
);

/** @brief LinSM_GotoSleepConfirmation
 * 
 * @details The LinIf will call this callback when the wake up signal command \n
 *          is sent not successfully/successfully on the network. \n
 *          Software Design Specification: [SWS_LinSM_00132]
 * 
 * @param [in]  network identification of the LIN channel
 * @param [in]  success true if wakeup was successfully sent, false otherwise
 */
void LinSM_WakeupConfirmation (
	NetworkHandleType network,
	boolean success
);

#endif /* LINSM_H */

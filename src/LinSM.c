/* AUTOSAR CP R20-11*/

#include "LinSM.h"
#include <stddef.h>


/* [SWS_LinSM_00208] */
/* [SWS_LinSM_00019] - done, we use only one network, can be done by using arrays instead singel variables */
/* [SWS_LinSM_00175] - done, we use only one network, can be done by using arrays instead singel variables */
/* [SWS_LinSM_00021] - done, we use only one network, can be done by using arrays instead singel variables */

#define LINSM_CONFIRMATION_TIMEOUT 100 

static LinSM_StateType LinSM_State                          = LINSM_UNINIT;         /* [SWS_LinSM_00161] */
static LinSM_ModeType LinSM_SubState                        = LINSM_NO_COM;
static LinSM_Full_Com_SubStateType LinSM_Full_Com_SubState  = LINSM_RUN_COMMUNICATION; 

static LinIf_SchHandleType LinSM_ScheduleTable;

/* [SWS_LinSM_00175] */
static uint16 GotoSleepTimer;
static uint16 WakeupTimer;
static uint16 ScheduleRequestTimer;


/* [SWS_LinSM_00155] */
void LinSM_Init(const LinSM_ConfigType* ConfigPtr)
{
    LinSM_State     = LINSM_INIT;   /* [SWS_LinSM_00025] */
    LinSM_SubState  = LINSM_NO_COM; /* [SWS_LinSM_00152] [SWS_LinSM_00160] */

    /* [SWS_LinSM_00216] */
    LinSM_ScheduleTable     = NULL_SCHEDULE;

    /* [SWS_LinSM_00043] */ 
    GotoSleepTimer          = 0;
    WakeupTimer             = 0;
    ScheduleRequestTimer    = 0;

    /* [SWS_LinSM_00166] - Done */
    /* [SWS_LinSM_00204] - Done */
    /* [SWS_LinSM_00151] - Done */
}


/* [SWS_LinSM_00113] */
Std_ReturnType LinSM_ScheduleRequest(NetworkHandleType network, LinIf_SchHandleType schedule)
{   
    Std_ReturnType ret_val = E_NOT_OK;

    // The following 3 reqs are for error handling
    /* [SWS_LinSM_00114] */
    if(network > 1){
        Det_ReportError(0,0,LINSM_SCHEDULE_REQUEST_SERVICE_ID,LINSM_E_NONEXISTENT_NETWORK);
        return E_NOT_OK;
    }
    /* [SWS_LinSM_00115] */
    if(schedule == NULL_SCHEDULE){
        Det_ReportError(0,0,LINSM_SCHEDULE_REQUEST_SERVICE_ID,LINSM_E_PARAMETER);
        return E_NOT_OK;
    }
    /* [SWS_LinSM_00116] */
    if(LinSM_State == LINSM_UNINIT){
        Det_ReportError(0,0,LINSM_SCHEDULE_REQUEST_SERVICE_ID,LINSM_E_UNINIT);
        return E_NOT_OK;
    }

    /* [SWS_LinSM_10211] */
    if(LinSM_SubState != LINSM_FULL_COM){
        return E_NOT_OK;
    } else {
        ScheduleRequestTimer = LINSM_CONFIRMATION_TIMEOUT;              /* [SWS_LinSM_00100] */
        ret_val = LinIf_ScheduleRequest(network, schedule);             /* [SWS_LinSM_00079] */
        if(ret_val == E_NOT_OK){
            BswM_LinSM_CurrentSchedule(network, LinSM_ScheduleTable);   /* [SWS_LinSM_00213] */
            ScheduleRequestTimer = 0;
        }
        return ret_val;   /* [SWS_LinSM_00168] */
    }

    /* [SWS_LinSM_00241] - only master implemented */
}

/* [SWS_LinSM_00117] */
void LinSM_GetVersionInfo ( Std_VersionInfoType* versioninfo) // in Std_Types.h
{
    /* [SWS_LinSM_00119] */
    if (versioninfo == NULL){
        Det_ReportError(0,0,LINSM_GET_VERSION_INFO_SERVICE_ID,LINSM_E_PARAM_POINTER);
    } 
}

/* [SWS_LinSM_00122] */
Std_ReturnType LinSM_GetCurrentComMode ( NetworkHandleType network, ComM_ModeType* mode)
{
    /* [SWS_LinSM_00123] */
    if (network > 1){
        Det_ReportError(0,0,LINSM_GET_CURR_COMM_MOD_SERVICE_ID,LINSM_E_NONEXISTENT_NETWORK);
        return E_NOT_OK;
    }

    /* [SWS_LinSM_00124] */ 
    if ( mode == NULL ){
        Det_ReportError(0,0,LINSM_GET_CURR_COMM_MOD_SERVICE_ID,LINSM_E_PARAM_POINTER);
        return E_NOT_OK;
    }

    /* [SWS_LinSM_00182] */ 
    /* [SWS_LinSM_00125] */
    if ( LinSM_State == LINSM_UNINIT ){
        Det_ReportError(0,0,LINSM_GET_CURR_COMM_MOD_SERVICE_ID,LINSM_E_UNINIT);
        *mode = COMM_NO_COMMUNICATION;
        return E_NOT_OK;
    }

    if (LinSM_SubState == LINSM_NO_COM) { 
        *mode = COMM_NO_COMMUNICATION;      /* [SWS_LinSM_00180] */
    } else if (LinSM_SubState == LINSM_FULL_COM){
        *mode = COMM_FULL_COMMUNICATION;    /* [SWS_LinSM_00181] */
    }

    return E_OK;
}


/* [SWS_LinSM_00126] */
Std_ReturnType LinSM_RequestComMode(NetworkHandleType network, ComM_ModeType mode)
{   
    // The following two reqs are for error handling
    /* [SWS_LinSM_00127] */
    if (network > 1){
        Det_ReportError(0,0,LINSM_REQ_COMM_MODE_SERVICE_ID,LINSM_E_NONEXISTENT_NETWORK);
        return E_NOT_OK;
    }

    /* [SWS_LinSM_00191] */
    if ( mode > 3 ){
        Det_ReportError(0,0,LINSM_REQ_COMM_MODE_SERVICE_ID,LINSM_E_PARAMETER);
        return E_NOT_OK;
    }
    
    /* [SWS_LinSM_00128] */
    if ( (LinSM_State == LINSM_UNINIT) ){
        Det_ReportError(0,0,LINSM_REQ_COMM_MODE_SERVICE_ID,LINSM_E_UNINIT);
        return E_NOT_OK;
    }

    /* [SWS_LinSM_00183] */
    if (mode == COMM_SILENT_COMMUNICATION){
        return E_NOT_OK;
    }

    switch(mode)
    {
        case COMM_NO_COMMUNICATION: //go to sleep
            if ( LinSM_SubState == LINSM_FULL_COM && LinSM_Full_Com_SubState == LINSM_RUN_COMMUNICATION){
                
                GotoSleepTimer = LINSM_CONFIRMATION_TIMEOUT;    /* [SWS_LinSM_00100] */

                if(LinIf_GotoSleep(network) == E_OK){           /* [SWS_LinSM_00035] [SWS_LinSM_00036] [SWS_LinSM_10208] */
                    LinSM_Full_Com_SubState = LINSM_GOTO_SLEEP; /* [SWS_LinSM_00302] */
                    return E_OK;
                } else {
                    GotoSleepTimer = 0;
                    ComM_BusSM_ModeIndication(network, COMM_FULL_COMMUNICATION);    /* [SWS_LinSM_00177] */
                    BswM_LinSM_CurrentState(network, LINSM_FULL_COM);  
                    return E_NOT_OK;
                }
            }
            break;
        case COMM_FULL_COMMUNICATION: //wake up
            
            WakeupTimer = LINSM_CONFIRMATION_TIMEOUT;   /* [SWS_LinSM_00100] */ 

            if ( LinIf_Wakeup(network) == E_OK){        /* [SWS_LinSM_00047] */
                return E_OK;
            } else {
                WakeupTimer = 0;
                return E_NOT_OK;                        /* [SWS_LinSM_00176] */
            }
            break;
        default:
            break;
    }

    /* [LinSM_10209] - Done */
    /* [LinSM_00178] - Done */
}

// --------------------------------------------------------------//
// ------------------------ Callbacks ---------------------------//
// --------------------------------------------------------------//

/* [SWS_LinSM_00129] */
// Called by LinIF
void LinSM_ScheduleRequestConfirmation(NetworkHandleType network, LinIf_SchHandleType schedule) /* [SWS_LinSM_00207] */
{  
    // The following two reqs are for error handling
    /* [SWS_LinSM_00130] */
    if (network > 1){
        Det_ReportError(0,0,LINSM_SCHEDULE_REQ_CONF_SERVICE_ID,LINSM_E_NONEXISTENT_NETWORK);
        return;
    }

    /* [SWS_LinSM_00131] */
    if ( (LinSM_State == LINSM_UNINIT) ){
        Det_ReportError(0,0,LINSM_SCHEDULE_REQ_CONF_SERVICE_ID,LINSM_E_UNINIT);
        return;
    }

    if(ScheduleRequestTimer > 0){  //No timeout
        LinSM_ScheduleTable = schedule;
        BswM_LinSM_CurrentSchedule(network,schedule);   /* [SWS_LinSM_00206] [SWS_LinSM_00207] */
        ScheduleRequestTimer = 0;                       /* [SWS_LinSM_00154] */
    }
}


/* [SWS_LinSM_91000] */
// Called by LinIF, only in slave, only master implemented
void LinSM_GotoSleepIndication ( NetworkHandleType Channel)
{
   // The following two reqs are for error handling
   /* [SWS_LinSM_00239] */
   if (Channel > 1){
       Det_ReportError(0,0,LINSM_GOTO_SLEEP_IND_SERVICE_ID,LINSM_E_NONEXISTENT_NETWORK);
       return;
   }

   /* [SWS_LinSM_00240] */
   if (LinSM_State == LINSM_UNINIT){
       Det_ReportError(0,0,LINSM_GOTO_SLEEP_IND_SERVICE_ID,LINSM_E_UNINIT);
       return;
   }
}


/* [SWS_LinSM_00135] */
// Called by LinIF
void LinSM_GotoSleepConfirmation (NetworkHandleType network, boolean success)
{
    // The following two reqs are for error handling
    /* [SWS_LinSM_00136] */
    if (network > 1){
        Det_ReportError(0,0,LINSM_GOTO_SLEEP_CONF_SERVICE_ID,LINSM_E_NONEXISTENT_NETWORK);
        return;
    }

    /* [SWS_LinSM_00137] */
    if ( (LinSM_State == LINSM_UNINIT) ){
        Det_ReportError(0,0,LINSM_GOTO_SLEEP_CONF_SERVICE_ID,LINSM_E_UNINIT);
        return;
    }

    /* [SWS_LinSM_00046] */
    if (GotoSleepTimer > 0 ){ // No timeout
        if (LinSM_SubState == LINSM_FULL_COM && LinSM_Full_Com_SubState == LINSM_GOTO_SLEEP){
            LinSM_SubState = LINSM_NO_COM;
            ComM_BusSM_ModeIndication(network, COMM_NO_COMMUNICATION);  /* [SWS_LinSM_00027] */
            BswM_LinSM_CurrentState(network, LINSM_NO_COM);             /* [SWS_LinSM_00193] */
        }
        GotoSleepTimer = 0; /* [SWS_LinSM_00154] */
    }

}


/* [SWS_LinSM_00132] */
// Called by LinIF
void LinSM_WakeupConfirmation (NetworkHandleType network, boolean success)
{
    // The following two reqs are for error handling
    /* [SWS_LinSM_00133] */
    if (network > 1){
        Det_ReportError(0,0,LINSM_WAKEUP_CONF_SERVICE_ID,LINSM_E_NONEXISTENT_NETWORK);
        return;
    }

    /* [SWS_LinSM_00134] */
    if ( (LinSM_State == LINSM_UNINIT) ){
        Det_ReportError(0,0,LINSM_WAKEUP_CONF_SERVICE_ID,LINSM_E_UNINIT);
        return;
    }

    if(WakeupTimer > 0){ // No timeout
        if(success){
            LinSM_SubState          = LINSM_FULL_COM;                   /* [SWS_LinSM_00049] */
            LinSM_Full_Com_SubState = LINSM_RUN_COMMUNICATION;          /* [SWS_LinSM_00301] */
            ComM_BusSM_ModeIndication(network, COMM_FULL_COMMUNICATION);/* [SWS_LinSM_00033] */
            BswM_LinSM_CurrentState(network, LINSM_FULL_COM);           /* [SWS_LinSM_00192] */
        } else{
            ComM_BusSM_ModeIndication(network, COMM_NO_COMMUNICATION);  /* [SWS_LinSM_00202] */
            BswM_LinSM_CurrentState(network, LINSM_NO_COM);  
        }
        WakeupTimer = 0; /* [SWS_LinSM_00154] */
    }
}


/* [SWS_LinSM_00162] */
/* [SWS_LinSM_00159] */
void LinSM_MainFunction(void) 
{
    uint8 channel=1;

    if (ScheduleRequestTimer > 0){
        ScheduleRequestTimer--;
    } else {                                                                                /* [SWS_LinSM_00101] */
        BswM_LinSM_CurrentSchedule(channel, LinSM_ScheduleTable);                           /* [SWS_LinSM_00214] */
        Det_ReportError(0,0,LINSM_MAIN_FUNCTION_SERVICE_ID,LINSM_E_CONFIRMATION_TIMEOUT);   /* [SWS_LinSM_00102] */
    }

    if (GotoSleepTimer > 0){
        GotoSleepTimer--;
    } else {                                                                                /* [SWS_LinSM_00101] */
        Det_ReportError(0,0,LINSM_MAIN_FUNCTION_SERVICE_ID,LINSM_E_CONFIRMATION_TIMEOUT);   /* [SWS_LinSM_00102] */
    }
    
    if (WakeupTimer > 0){
        WakeupTimer--;
    } else {                                                                                /* [SWS_LinSM_00101] */
       BswM_LinSM_CurrentState(channel,LINSM_NO_COM);                                       /* [SWS_LinSM_00215] */
       ComM_BusSM_ModeIndication(channel,COMM_NO_COMMUNICATION);                            /* [SWS_LinSM_00170] */
       Det_ReportError(0,0,LINSM_MAIN_FUNCTION_SERVICE_ID,LINSM_E_CONFIRMATION_TIMEOUT);    /* [SWS_LinSM_00102] */
    }
}

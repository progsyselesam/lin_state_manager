#ifndef LINIF_H
#define LINIF_H


/** @file LinIf.h
 *
 * @brief Definition of types
 *
 * @details This document specifies the functionality, API and the configuration \n
 *          of the AUTOSAR Basic Software module LIN Interface (LinIf) \n
 *          and the LIN Transport Protocol (LIN TP, LinTp). \n
 *          The LIN TP is a part of the LIN Interface.
 *
 * @see "Specification of LIN Interface" AUTOSAR CP R20-11
 */


#include "Std_Types.h"
#include "ComStack_Types.h"


#define NULL_SCHEDULE 0x00


/** @brief LinIf_SchHandleType
 *
 * @details Index of the schedule table that is selectable and followed by LIN Interface. \n
 *          Value is unique per LIN channel/controller, but not per ECU. \n
 *          The number of schedule tables is limited to 255. \n
 *          Software Design Specification: [SWS_LinIf_00197]
 */
typedef uint8 LinIf_SchHandleType;


/** @brief LinIf_GotoSleep
 *
 * @details Initiates a transition into the Sleep Mode on the selected channel. \n
 *          Software Design Specification: [SWS_LinIf_00204]
 *
 * @param [in] Channel Identification of the LIN channel.
 *
 * @return  E_OK        Request to go to sleep has been accepted or sleep transition \n
 *                      is already in progress or controller is already in sleep state. \n
 *          E_NOT_OK    Request to go to sleep has not been accepted due to one or more of the following reasons: \n
 *                          - LIN Interface has not been initialized \n
 *                          - referenced channel does not exist (identification is out of range)
 */
Std_ReturnType LinIf_GotoSleep (
    NetworkHandleType Channel
);


/** @brief LinIf_ScheduleRequest
 *
 * @details Requests a schedule table to be executed. Only used for LIN master nodes. \n
 *          Software Design Specification: [SWS_LinIf_00202]
 *
 * @param [in] Channel Channel index.
 * @param [in] Schedule Identification of the new schedule to be set.
 *
 * @return  E_OK        Schedule table request has been accepted. \n
 *          E_NOT_OK    Schedule table switch request has not been accepted due to one of the following reasons: \n
 *                          - LIN Interface has not been initialized \n
 *                          - referenced channel does not exist (identification is out of range) \n
 *                          - referenced schedule table does not exist (identification is out of range) \n
 *                          - State is sleep
 */
Std_ReturnType LinIf_ScheduleRequest (
    NetworkHandleType Channel,
    LinIf_SchHandleType Schedule
);


/** @brief LinIf_Wakeup
 *
 * @details Initiates the wake up process. \n
 *          Software Design Specification: [SWS_LinIf_00205]
 *
 * @param [in]  Channel Identification of the LIN channel.
 * 
 * @return  E_OK        Request to wake up has been accepted or the controller is not in sleep state. \n
 *          E_NOT_OK    Request to wake up has not been accepted due to one or more of the following reasons: \n
 *                          - LIN Interface has not been initialized \n
 *                          - referenced channel does not exist (identification is out of range) \n
 *                          - Lin_Wakeup has returned E_NOT_OK \n
 *                          - Lin_WakeupInternal has returned E_NOT_OK
 */
Std_ReturnType LinIf_Wakeup (
    NetworkHandleType Channel
);


#endif /* LINIF_H */

#include "LinIf.h"


Std_ReturnType LinIf_GotoSleep (
    NetworkHandleType Channel
){}


Std_ReturnType LinIf_ScheduleRequest (
    NetworkHandleType Channel,
    LinIf_SchHandleType Schedule
){}


Std_ReturnType LinIf_Wakeup (
    NetworkHandleType Channel
){}

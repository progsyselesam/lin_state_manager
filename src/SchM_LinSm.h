#ifndef SCHM_LINSM_H
#define SCHM_LINSM_H


/** @file SchM_LinSm.h
 * 
 * @brief Scheduled Functions
 * 
 * @details This file lists the functions that are called with a fixed period.
 * 
 * @see "Specification of LIN State Manager" AUTOSAR CP R20-11
 */ 


/** @brief LinSM_MainFunction
 * 
 * @details Periodic function that runs the timers of different request timeouts. \n
 *          Software Design Specification: [SWS_LinSM_00156]
 */ 
void LinSM_MainFunction (
	void
);

#endif /* SCHM_LINSM_H */

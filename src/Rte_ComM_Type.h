#ifndef RTE_COMM_TYPE_H
#define RTE_COMM_TYPE_H


/** @file Rte_ComM_Type.h
 * 
 * @brief Definition of types  
 * 
 * @details The Communication Manager Module(COM Manager, ComM) is a component \n
 *          of the Basic Software (BSW). The purpose of the ComM module is: \n
 *          Simplifying the usage of the bus communication stack for the user. \n
 *          This includes a simplified network management handling. \n
 *          Coordinating the availability of the bus communication stack \n
 *          (allow sending and receiving of signals) of multiple independent \n
 *          software components on one ECU.
 * 
 * @see "Specification of Communication Manager" AUTOSAR CP R20-11
 */ 

#include "Std_Types.h"

#define COMM_NO_COMMUNICATION                       0
#define COMM_SILENT_COMMUNICATION                   1
#define COMM_FULL_COMMUNICATION                     2
#define COMM_FULL_COMMUNICATION_WITH_WAKEUP_REQUEST 3


/** @brief ComM_ModeType    
 * 
 * @details Current mode of the Communication Manager (main state of the state machine). \n
 *          Software Design Specification: [SWS_ComM_00672]
 */ 
typedef uint8 ComM_ModeType;


#endif /* RTE_COMM_TYPE_H */

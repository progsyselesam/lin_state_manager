#ifndef COMM_BUSSM_H
#define COMM_BUSSM_H


/** @file ComM_BusSM.h
 *
 * @brief Function declaration
 *
 * @details This is a functions provided for other modules. \n
 *          The function prototypes of ComM callback declarations.
 *
 * @see "Specification of Communication Manager" AUTOSAR CP R20-11
 */

#include "Rte_ComM_Type.h"
#include "ComStack_Types.h"


/** @brief ComM_BusSM_ModeIndication
 *
 * @details Indication of the actual bus mode by the corresponding Bus State Manager. \n
 *          ComM shall propagate the indicated state to the users with means of the RTE and BswM. \n
 *          Software Design Specification: [SWS_ComM_00675]
 *
 * @param [in]  Channel See NetworkHandleType
 * @param [in]  ComMode See ComM_ModeType
 */
void ComM_BusSM_ModeIndication (
    NetworkHandleType Channel,
    ComM_ModeType ComMode
);


/** @brief ComM_BusSM_BusSleepMode
 *
 * @details Notification of the corresponding Bus State Manager that the actual bus mode \n
 *          is Bus-Sleep. Only applicable for ComM channels with ComMNmVariant set to SLAVE_ACTIVE \n
 *          or SLAVE_PASSIVE.mE.g. LIN slaves (ComMNMVariant = SLAVE_ACTIVE) or Ethernet channels \n
 *          with OA TC10 compliant Ethernet hardware which act as passive communication slave \n
 *          (ComMNMVariant = SLAVE_PASSIVE and EthTrcvActAsSlavePassiveEnabled set to TRUE). \n
 *          Software Design Specification: [SWS_ComM_91000]
 *
 * @param [in]  Channel Identification of the channel
 */
void ComM_BusSM_BusSleepMode (
    NetworkHandleType Channel
);


#endif /* COMM_BUSSM_H */
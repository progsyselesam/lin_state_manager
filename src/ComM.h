#ifndef COMM_H
#define COMM_H


/** @file ComM.h
 *
 * @brief Function declaration
 *
 * @details The Communication Manager Module(COM Manager, ComM) is a component \n
 *          of the Basic Software (BSW). The purpose of the ComM module is: \n
 *          Simplifying the usage of the bus communication stack for the user. \n
 *          This includes a simplified network management handling. \n
 *          Coordinating the availability of the bus communication stack \n
 *          (allow sending and receiving of signals) of multiple independent \n
 *          software components on one ECU.
 *
 * @see "Specification of Communication Manager" AUTOSAR CP R20-11
 */


#include "Rte_ComM_Type.h"
#include "ComStack_Types.h"


#endif /* COMM_H */

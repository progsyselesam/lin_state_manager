#ifndef COMSTACK_TYPES_H
#define COMSTACK_TYPES_H


/** @file ComStack_Types.h
 * 
 * @brief Definition of types
 * 
 * @details This document specifies the AUTOSAR communication stack typeheader file. \n
 *          It contains all types that are used across several modules \n
 *          of the communication stack of the basic software and all types \n
 *          of all basic software modules that are platform and compiler independent.
 * 
 * @see "Specification of Communication Stack Types" AUTOSAR CP R20-11
 */ 


#include "Platform_Types.h"


/** @brief NetworkHandleType
 * 
 * @details Variables of the type NetworkHandleType shall be used to store \n
 *          the identifier of a communication channel. \n
 *          Software Design Specification: [SWS_COMTYPE_00038]
 */ 
typedef uint8 NetworkHandleType;


#endif /* COMSTACK_TYPES_H */

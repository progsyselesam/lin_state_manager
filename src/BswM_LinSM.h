#ifndef BSWM_LINSM_H
#define BSWM_LINSM_H


/** @file BswM_LinSM.h
 *
 * @brief Function declaration
 *
 * @details This file specifies the API and the configuration of the \n
 *          AUTOSAR Basic Software module BSW Mode Manager (BswM).
 *
 * @see "Specification of Basic Software Mode Manager" AUTOSAR CP R20-11
 */


#include "ComStack_Types.h"
#include "LinSM.h"
#include "LinIf.h"


/** @brief BswM_LinSM_CurrentSchedule
 *
 * @details Function called by LinSM to indicate the currently active schedule table for a specific LIN channel. \n
 *          Software Design Specification: [SWS_BswM_00058]
 *
 * @param [in]  Network The LIN channel that the schedule table switch have occurred on.
 * @param [in]  CurrentSchedule The currently active schedule table of the LIN channel.
 */
void BswM_LinSM_CurrentSchedule (
    NetworkHandleType Network,
    LinIf_SchHandleType CurrentSchedule
);


/** @brief BswM_LinSM_CurrentState
 *
 * @details Function called by LinSM to indicate its current state. \n
 *          Software Design Specification: [SWS_BswM_00052]
 *
 * @param [in]  Network The LIN channel that the indicated state corresponds to.
 * @param [in]  CurrentState The current state of the LIN channel.
 */
void BswM_LinSM_CurrentState (
    NetworkHandleType Network,
    LinSM_ModeType CurrentState
);


#endif /* BSWM_LINSM_H */

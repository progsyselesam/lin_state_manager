unit-test: 
	@echo "Unit test..."
	@mkdir -p build
	@gcc ./src/UT_LinSM.c -o ./build/UT_LinSM.exe
	@./build/UT_LinSM.exe

clean:
	@echo "Clean..."
	@rm -rf build
	@rm -rf Report_Gcov
	@rm -rf Report_CppCheck
	@rm -rf Documentation

coverage:
	@echo "Coverage..."
	@mkdir -p Report_Gcov
	@cd Report_Gcov && gcc -fprofile-arcs -ftest-coverage -g ../src/UT_LinSM.c -o ./UT_LinSM.exe
	@./Report_Gcov/UT_LinSM.exe
	@cd Report_Gcov && gcov ./UT_LinSM.c
	@gcovr

static:
	@echo "Static analysis..."
	@mkdir -p Report_CppCheck
	@cppcheck --addon=cert ./src/LinSM.c
	@cd Report_CppCheck && cppcheck --xml --xml-version=2 --addon=cert ../src/LinSM.c 2>report-LinSM.xml

main:
	@echo "Compiling..."
	@mkdir -p build
	@cd src && gcc Det.c BswM_LinSM.c ComM_BusSM.c ComM.c LinIf.c LinSM.c -o ../build/LinSM.exe

documentation:
	@echo "Generating documentation..."
	@doxygen
# Project of LIN State Manager

Implementation of the Autosar specification: LIN State Manager.  

## Build and run for Linux
Compilation LinSM.c  
```
gcc LinSM.c -o LinSM.exe
./LinSM.exe
```

Unit tests for LinSM.c library  
```
gcc UT_LinSM.c -o UT_LinSM.exe
./UT_LinSM.exe
```

## Makefile 
Available command:  
  
* make unit-test        - Unit testing for LinSM.c  
* make coverage         - Coverage analysis for LinSM.c  
* make static           - Static analysis for LinSM.c  
* make main             - Compiling for LinSM.c  
* make documentation    - Generating documentation  
* make clean            - Cleaning of result files, analysis reports and documentation  

var indexSectionsWithContent =
{
  0: "bcdlnrstu",
  1: "ls",
  2: "bcdlrsu",
  3: "bcdlt",
  4: "cln",
  5: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "typedefs",
  5: "enums"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Typedefs",
  5: "Enumerations"
};

